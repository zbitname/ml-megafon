const groupedData = require('./result.json');

module.exports.getResult = rows => {
  const results = [];

  for (const row of rows) {
    const resultForRow = [];

    for (const name in row) {
      if (name === 'SK_ID') {
        continue;
      }

      if (groupedData[name]['0'].indexOf(String(row[name])) >= 0) {
        resultForRow.push(0);
      }
  
      if (groupedData[name]['1'].indexOf(String(row[name])) >= 0) {
        resultForRow.push(1);
      }
    }

    if (resultForRow.length) {
      results.push(resultForRow.reduce((prev, cur) => prev + cur, 0) * 1.05 / resultForRow.length);
    }
  }

  if (!results.length) {
    return null;
  }

  const rate = results.reduce((prev, cur) => prev + cur, 0) / results.length;

  return rate > 1 ? 1 : rate;
};
