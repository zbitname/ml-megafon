const path = require('path');
const fs = require('fs');

const helpers = require('../../helpers');
const CSIBySKID = require('../csi_by_skid');

const data = helpers.readCSV(
  path.normalize(path.join(__dirname, '../../condition_csi/csi_analyze/train/subs_features_train.csv')),
  {
    'SNAP_DATE': String,
    'COM_CAT#1': helpers.parseNumber,
    'SK_ID': Number,
    'COM_CAT#2': helpers.parseNumber,
    'COM_CAT#3': helpers.parseNumber,
    'BASE_TYPE': helpers.parseNumber,
    'ACT': helpers.parseNumber,
    'ARPU_GROUP': helpers.parseNumber,
    'COM_CAT#7': helpers.parseNumber,
    'COM_CAT#8': helpers.parseNumber,
    'DEVICE_TYPE_ID': helpers.parseNumber,
    'INTERNET_TYPE_ID': helpers.parseNumber,
    'REVENUE': helpers.parseNumber,
    'ITC': helpers.parseNumber,
    'VAS': helpers.parseNumber,
    'RENT_CHANNEL': helpers.parseNumber,
    'ROAM': helpers.parseNumber,
    'COST': helpers.parseNumber,
    'COM_CAT#17': helpers.parseNumber,
    'COM_CAT#18': helpers.parseNumber,
    'COM_CAT#19': helpers.parseNumber,
    'COM_CAT#20': helpers.parseNumber,
    'COM_CAT#21': helpers.parseNumber,
    'COM_CAT#22': helpers.parseNumber,
    'COM_CAT#23': helpers.parseNumber,
    'COM_CAT#24': helpers.parseNumber,
    'COM_CAT#25': helpers.parseNumber,
    'COM_CAT#26': helpers.parseNumber,
    'COM_CAT#27': helpers.parseNumber,
    'COM_CAT#28': helpers.parseNumber,
    'COM_CAT#29': helpers.parseNumber,
    'COM_CAT#30': helpers.parseNumber,
    'COM_CAT#31': helpers.parseNumber,
    'COM_CAT#32': helpers.parseNumber,
    'COM_CAT#33': helpers.parseNumber,
    'COM_CAT#34': helpers.parseNumber
  }
);

for (const item of data) {
  item.CSI = CSIBySKID[item.SK_ID];
}

const results = {};

for (const item of data) {
  for (const name in item) {
    if (name === 'SK_ID' || name === 'CSI') {
      continue;
    }

    if (results[name]) {
      if (results[name][item.CSI]) {
        if (results[name][item.CSI][item[name]]) {
          results[name][item.CSI][item[name]]++;
        } else {
          results[name][item.CSI][item[name]] = 1;
        }
      } else {
        results[name][item.CSI] = {[item[name]]: 1};
      }
    } else {
      results[name] = {[item.CSI]: {[item[name]]: 1}};
    }
  }
}

fs.writeFileSync(path.normalize(path.join(__dirname, './result.json')), JSON.stringify(results, null, 2));
