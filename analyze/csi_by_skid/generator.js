const helpers = require('../../helpers');
const path = require('path');
const fs = require('fs');

const data = helpers.readCSV(
  path.normalize(path.join(__dirname, '../../condition_csi/csi_analyze/train/subs_csi_train.csv')),
  {
    SK_ID: Number,
    CSI: Number
  }
);

const CSIByClient = {};

for (const row of data) {
  CSIByClient[row.SK_ID] = row.CSI;
}

fs.writeFileSync(path.normalize(path.join(__dirname, './result.json')), JSON.stringify(CSIByClient));
