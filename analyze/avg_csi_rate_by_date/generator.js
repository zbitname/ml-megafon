const helpers = require('../../helpers');
const path = require('path');
const fs = require('fs');

const data = helpers.readCSV(
  path.normalize(path.join(__dirname, '../../condition_csi/csi_analyze/train/subs_csi_train.csv')),
  {
    SK_ID: Number,
    CSI: Number,
    CONTACT_DATE: v => Number(v.split('.')[0])
  }
);

const CSIByDate = {};

for (const row of data) {
  if (CSIByDate[row.CONTACT_DATE]) {
    CSIByDate[row.CONTACT_DATE].push(row.CSI);
  } else {
    CSIByDate[row.CONTACT_DATE] = [row.CSI];
  }
}

const result = {};

for (let date in CSIByDate) {
  const itemData = CSIByDate[date];
  result[date] = {
    rate: itemData.reduce((prev, cur) => prev + cur, 0) / itemData.length,
    count: itemData.length
  };
}

fs.writeFileSync(path.normalize(path.join(__dirname, './result.json')), JSON.stringify(result));
