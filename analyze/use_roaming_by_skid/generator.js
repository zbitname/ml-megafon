const helpers = require('../../helpers');
const path = require('path');
const fs = require('fs');

const CSIBySKID = require('../csi_by_skid');

const data = helpers.readCSV(
  path.normalize(path.join(__dirname, '../../condition_csi/csi_analyze/train/subs_features_train.csv')),
  {
    SK_ID: Number,
    ROAM: helpers.parseNumber
  }
);

const ROAMBySKID = {};

for (const row of data) {
  if (ROAMBySKID[row.SK_ID]) {
    ROAMBySKID[row.SK_ID].push(row.ROAM);
  } else {
    ROAMBySKID[row.SK_ID] = [row.ROAM];
  }
}

const result = {};

for (const skid in ROAMBySKID) {
  result[skid] = {
    csi: CSIBySKID[skid],
    useRoam: ROAMBySKID[skid].some(Boolean),
    sumOfSpentToRoam: ROAMBySKID[skid].reduce((prev, cur) => prev + cur, 0)
  };
}

fs.writeFileSync(path.normalize(path.join(__dirname, './result.json')), JSON.stringify(result));
