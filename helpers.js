const fs = require('fs');

const MS_TO_DAY_RATE = 3600 * 24 * 1000;

const readCSV = (path, schema) => {
  const data = fs.readFileSync(path).toString().split('\n').filter(r => Boolean(r)).map(r => r.split(';'));
  const head = data[0];

  data.splice(0, 1);

  const result = [];

  for (const row of data) {
    const obj = {};

    for (let i = 0; i < head.length; i++) {
      const name = head[i];

      if (schema && schema[name]) {
        obj[name] = schema[name](row[i]);
      }
    }

    result.push(obj);
  }

  return result;
};

const parseNumber = v => Number(v.replace(',', '.'));

const buildResultCSV = (path, data) => {
  // const result = ['SK_ID;CSI;CONTACT_DATE'];
  const result = [];

  for (const row of data) {
    result.push(row.CSI);
  }

  fs.writeFileSync(path, result.join('\n'));
};

const writeCSV = (path, head, data) => {
  const result = [head.join(';')];

  for (const set of data) {
    const row = [];

    for (const key of head) {
      row.push(set[key]);
    }

    result.push(row.join(';'));
  }

  fs.writeFileSync(path, result.join('\n'));
};

const writeFromArraysCSV = (path, head, data) => {
  const result = [head];

  for (const row of data) {
    result.push(row);
  }

  fs.writeFileSync(path, result.join('\n'));
};

const convertDateToInt = d => {
  const date = new Date(0);
  const parsedDate = d.split('.').map(Number);

  date.setDate(parsedDate[0]);
  date.setMonth(parsedDate[1]);

  if (!parsedDate[2]) {
    if (parsedDate[1] > 5) {
      parsedDate[2] = 1;
    } else {
      parsedDate[2] = 2;
    }
  }

  date.setFullYear(parsedDate[2] + 2000);

  return Math.round(date / MS_TO_DAY_RATE);
};

const convertDateTimeHourToInt = d => {
  const splitted = d.split(' ');
  const dateInt = convertDateToInt(splitted[0]) * 24;
  const hour = splitted[1].split(':')[0];

  return dateInt + hour;
};

module.exports.readCSV = readCSV;
module.exports.parseNumber = parseNumber;
module.exports.buildResultCSV = buildResultCSV;
module.exports.writeCSV = writeCSV;
module.exports.writeFromArraysCSV = writeFromArraysCSV;
module.exports.convertDateToInt = convertDateToInt;
module.exports.convertDateTimeHourToInt = convertDateTimeHourToInt;
