const tfjsCore = require('@tensorflow/tfjs-core');
const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node-gpu');
// require('@tensorflow/tfjs-node');

console.log(`BACKEND IS ${tf.getBackend()}`);

const y = require('../../condition_csi/v2/data/train/y.json');
const features = require('../../condition_csi/v2/data/train/features.json');

console.log(y.length, features.length);

const yForTrain = tf.tensor3d(y.map(v => [new Array(features[0][0].length).fill(v)]));

const featuresModel = tf.sequential();
const featuresForTrain = tf.tensor3d(features);

featuresModel.add(tf.layers.dense({units: 35, activation: 'sigmoid', inputShape: [features[0].length, features[0][0].length]}));

featuresModel.compile({
  loss: tf.losses.sigmoidCrossEntropy,
  optimizer: tf.train.sgd(0.07),
  metrics: tf.metrics.binaryAccuracy,
  initializer: 'glorotNormal'
});

(async () => {
  await featuresModel.fit(featuresForTrain, yForTrain, {
    epochs: 10,
    batchSize: 16,
    stepsPerEpoch: 1000,
    validationSteps: 1000,
    shuffle: true,
    validationSplit: .2
  });
})();
