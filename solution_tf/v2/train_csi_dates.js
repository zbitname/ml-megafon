const tfjsCore = require('@tensorflow/tfjs-core');
const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node-gpu');
// require('@tensorflow/tfjs-node');

console.log(`BACKEND IS ${tf.getBackend()}`);

const y = require('../../condition_csi/v2/data/train/y.json');
const csiDates = require('../../condition_csi/v2/data/train/csi_dates.json');

const yForTrain = tf.tensor1d(y);

const csiDatesModel = tf.sequential();
const csiDatesForTrain = tf.tensor1d(csiDates);

csiDatesModel.add(tf.layers.dense({units: 1, activation: 'sigmoid', inputShape: [1]}));

csiDatesModel.compile({
  loss: tf.losses.sigmoidCrossEntropy,
  optimizer: tf.train.sgd(0.07),
  metrics: tf.metrics.binaryAccuracy,
  initializer: 'glorotNormal'
});

(async () => {
  await csiDatesModel.fit(csiDatesForTrain, yForTrain, {
    epochs: 10,
    batchSize: 16,
    stepsPerEpoch: 1000,
    validationSteps: 1000,
    shuffle: true,
    validationSplit: .2
  });
})();
