const tfjsCore = require("@tensorflow/tfjs-core");
const common = require('@tensorflow/tfjs-layers/dist/backend/common');
const fs = require('fs');
const path = require('path');

const tf = require('@tensorflow/tfjs');
// require('@tensorflow/tfjs-node-gpu');
require('@tensorflow/tfjs-node');

const xTrain = require('../../condition_csi/v1/x_train.json').map(a => a.filter(v => typeof v === 'number')); // length = 47411
const yTrain = require('../../condition_csi/v1/y_train.json');

const xItemLength = xTrain[0].length;

const trainBatchSize = xTrain.length;

const xTest = require('../../condition_csi/v1/x_test.json').map(a => a.filter(v => typeof v === 'number'));;

console.log('xTrain', xTrain.length);
console.log('xTest', xTest.length);

// Build and compile model.
const model = tf.sequential();

const kernel = tf.variable(tf.tensor(new Array(xItemLength).fill(1)), true);

// https://js.tensorflow.org/api/0.13.3/#layers.dense
// model.add(tf.layers.batchNormalization({
//   inputShape: [xItemLength]
// }));

model.add(tf.layers.dense({
  units: 1,
  activation: 'sigmoid',
  inputShape: [xItemLength],
  // useBias: true,
  kernel,
  kernelInitializer: 'glorotNormal',
  kernelRegularizer: 'l1l2'
}));

model.add(tf.layers.activation({activation: 'sigmoid'}));

model.compile({
  // optimizer: 'rmsprop',
  optimizer: tf.train.adagrad(2.31, 0.1),
  loss: tf.losses.sigmoidCrossEntropy,
  metrics: tf.metrics.roc
});

// Generate some synthetic data for training.
const xs = tf.tensor2d(xTrain);
const ys = tf.tensor1d(yTrain);

(async () => {
  // Train model with fit().
  await model.fit(xs, ys, {
    epochs: 120,
    batchSize: 64
  });

  const xsTest = tf.tensor2d(xTest);
  // const ysTest = tf.tensor1d(yTest);

  // Run inference with predict().
  const result = model.predict(xsTest);

  const r = JSON.parse(JSON.stringify(result.toFloat().flatten().dataSync()));

  const rv = [];
  for (const k in r) {
    rv.push(r[k]);
  }

  console.log(rv.filter(v => v === 0).length);
  console.log(rv);
  console.log(kernel.toFloat().flatten().dataSync())

  fs.writeFileSync(path.normalize(path.join(__dirname, './y.csv')), rv.map(v => v.toFixed(20)).join('\n'));
})();
