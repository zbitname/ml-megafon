const tfjsCore = require('@tensorflow/tfjs-core');
const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node-gpu');
// require('@tensorflow/tfjs-node');

console.log(tf.getBackend());

const x = require('../../condition_csi/v1/x_train.json').map(a => a.filter(v => typeof v === 'number')); // length = 47411
const y = require('../../condition_csi/v1/y_train.json');

const xItemLength = x[0].length;

const trainBatchSize = Math.ceil(x.length / 1.2);

const xTrain = x.slice(0, trainBatchSize);
const yTrain = y.slice(0, trainBatchSize);

const xTest = x.slice(trainBatchSize);
const yTest = y.slice(trainBatchSize);

console.log('xTrain', xTrain.length);
console.log('xTest', xTest.length);

// Build and compile model.
const model = tf.sequential();
// https://js.tensorflow.org/api/0.13.3/#layers.dense
model.add(tf.layers.dense({
  units: 1,
  activation: 'sigmoid',
  inputShape: [xItemLength],
  // useBias: true,
  kernel: tf.variable(tf.tensor(new Array(xItemLength).fill(1)), true),
  kernelInitializer: 'glorotNormal',
  kernelRegularizer: 'l1l2'
}));
model.compile({optimizer: 'rmsprop', loss: tf.losses.huberLoss, metrics: tf.metrics.binaryAccuracy});

// Generate some synthetic data for training.
const xs = tf.tensor2d(xTrain);
const ys = tf.tensor1d(yTrain);

(async () => {
  // Train model with fit().
  await model.fit(xs, ys, {epochs: 10});

  const xsTest = tf.tensor2d(xTest);
  const ysTest = tf.tensor1d(yTest);

  // Run inference with predict().
  const result = model.predict(xsTest, ysTest);

  const r = JSON.parse(JSON.stringify(result.toFloat().flatten().dataSync()));

  const rv = [];
  for (const k in r) {
    rv.push(r[k]);
  }

  console.log(rv.filter(v => v === 0).length);
  console.log(rv);
})();
