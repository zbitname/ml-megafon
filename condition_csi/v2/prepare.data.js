// Можно отсеять features у которых features.SNAP_DATE > csi.CONTACT_DATE, т.к. нет особо большого смысла в данных о клиенте после того, как его опросили.

const path = require('path');
const fs = require('fs');

const helpers = require('../../helpers');

const DATA_TYPE = 'train';

// SK_ID;CSI;CONTACT_DATE
const csi = helpers.readCSV(path.normalize(path.join(__dirname, `../csi_analyze/${DATA_TYPE}/subs_csi_train.csv`)), {
  SK_ID: Number,
  CSI: Number,
  CONTACT_DATE: helpers.convertDateToInt
});

const csiMySKID = {};
for (const i of csi) {
  if (csiMySKID[i.SK_ID]) {
    throw new Error('SK_ID в выборке CSI встречается дважды. Непорядо...');
  }

  csiMySKID[i.SK_ID] = i;
}

{
  const result = [];

  fs.writeFileSync(path.normalize(path.join(__dirname, `./data/${DATA_TYPE}/csi_dates.json`)), JSON.stringify(csi.map(i => i.CONTACT_DATE)));

  if (DATA_TYPE === 'train') {
    fs.writeFileSync(path.normalize(path.join(__dirname, `./data/${DATA_TYPE}/y.json`)), JSON.stringify(csi.map(i => i.CSI)));
  }
}

// SNAP_DATE;COM_CAT#1;SK_ID;COM_CAT#2;COM_CAT#3;BASE_TYPE;ACT;ARPU_GROUP;COM_CAT#7;COM_CAT#8;DEVICE_TYPE_ID;INTERNET_TYPE_ID;REVENUE;ITC;VAS;RENT_CHANNEL;ROAM;COST;COM_CAT#17;COM_CAT#18;COM_CAT#19;COM_CAT#20;COM_CAT#21;COM_CAT#22;COM_CAT#23;COM_CAT#24;COM_CAT#25;COM_CAT#26;COM_CAT#27;COM_CAT#28;COM_CAT#29;COM_CAT#30;COM_CAT#31;COM_CAT#32;COM_CAT#33;COM_CAT#34
const features = helpers.readCSV(path.normalize(path.join(__dirname, `../csi_analyze/${DATA_TYPE}/subs_features_train.csv`)), {
  SNAP_DATE: helpers.convertDateToInt,
  SK_ID: Number,
  BASE_TYPE: Number,
  ACT: Number,
  ARPU_GROUP: Number,
  DEVICE_TYPE_ID: Number,
  INTERNET_TYPE_ID: Number,
  REVENUE: helpers.parseNumber,
  ITC: helpers.parseNumber,
  VAS: helpers.parseNumber,
  RENT_CHANNEL: helpers.parseNumber,
  ROAM: helpers.parseNumber,
  COST: helpers.parseNumber,

  'COM_CAT#1': Number,
  'COM_CAT#2': Number,
  'COM_CAT#3': Number,
  'COM_CAT#7': Number,
  'COM_CAT#8': Number,
  'COM_CAT#17': helpers.parseNumber,
  'COM_CAT#18': helpers.parseNumber,
  'COM_CAT#19': helpers.parseNumber,
  'COM_CAT#20': helpers.parseNumber,
  'COM_CAT#21': helpers.parseNumber,
  'COM_CAT#22': helpers.parseNumber,
  'COM_CAT#23': helpers.parseNumber,
  'COM_CAT#24': helpers.convertDateToInt,
  'COM_CAT#25': Number,
  'COM_CAT#26': Number,
  'COM_CAT#27': helpers.parseNumber,
  'COM_CAT#28': helpers.parseNumber,
  'COM_CAT#29': helpers.parseNumber,
  'COM_CAT#30': helpers.parseNumber,
  'COM_CAT#31': helpers.parseNumber,
  'COM_CAT#32': helpers.parseNumber,
  'COM_CAT#33': helpers.parseNumber,
  'COM_CAT#34': Number
});

const featuresBySKID = {};
for (const i of features) {
  if (featuresBySKID[i.SK_ID]) {
    featuresBySKID[i.SK_ID].push(i);
  } else {
    featuresBySKID[i.SK_ID] = [i]
  }
}

{
  const result = [];
  let maxLength = 0;

  for (const i of csi) {
    const r = featuresBySKID[i.SK_ID];

    if (r.length > maxLength) {
      maxLength = r.length;
    }

    result.push(r.map(v => {
      return [
        v.SNAP_DATE,
        // v.SK_ID,
        v.BASE_TYPE,
        v.ACT,
        v.ARPU_GROUP,
        v.DEVICE_TYPE_ID,
        v.INTERNET_TYPE_ID,
        v.REVENUE,
        v.ITC,
        v.VAS,
        v.RENT_CHANNEL,
        v.ROAM,
        v.COST,
        v['COM_CAT#1'],
        v['COM_CAT#2'],
        v['COM_CAT#3'],
        v['COM_CAT#7'],
        v['COM_CAT#8'],
        v['COM_CAT#17'],
        v['COM_CAT#18'],
        v['COM_CAT#19'],
        v['COM_CAT#20'],
        v['COM_CAT#21'],
        v['COM_CAT#22'],
        v['COM_CAT#23'],
        v['COM_CAT#24'],
        v['COM_CAT#25'],
        v['COM_CAT#26'],
        v['COM_CAT#27'],
        v['COM_CAT#28'],
        v['COM_CAT#29'],
        v['COM_CAT#30'],
        v['COM_CAT#31'],
        v['COM_CAT#32'],
        v['COM_CAT#33'],
        v['COM_CAT#34']
      ];
    }));
  }

  const zeros = new Array(result[0][0].length).fill(null);

  for (const i of result) {
    if (i.length < maxLength) {
      i.push(...new Array(maxLength - i.length).fill(zeros));
    }
  }

  fs.writeFileSync(path.normalize(path.join(__dirname, `./data/${DATA_TYPE}/features.json`)), JSON.stringify(result));
}
