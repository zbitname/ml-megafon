const path = require('path');
const helpers = require('../../helpers');
const fs = require('fs');

{
  const model = {
    // 'SK_ID': Number,
    'CONTACT_DATE': helpers.convertDateToInt,
    'SNAP_DATE': helpers.convertDateToInt,
    'BASE_TYPE': Number,
    'ACT': helpers.parseNumber,
    'ARPU_GROUP': helpers.parseNumber,
    'DEVICE_TYPE_ID': Number,
    'INTERNET_TYPE_ID': Number,
    'REVENUE': helpers.parseNumber,
    'ITC': helpers.parseNumber,
    'VAS': helpers.parseNumber,
    'RENT_CHANNEL': helpers.parseNumber,
    'ROAM': helpers.parseNumber,
    'COST': helpers.parseNumber,
    'COM_CAT#1': helpers.parseNumber,
    'COM_CAT#2': helpers.parseNumber,
    'COM_CAT#3': helpers.parseNumber,
    'COM_CAT#7': helpers.parseNumber,
    'COM_CAT#8': helpers.parseNumber,
    'COM_CAT#17': helpers.parseNumber,
    'COM_CAT#18': helpers.parseNumber,
    'COM_CAT#19': helpers.parseNumber,
    'COM_CAT#20': helpers.parseNumber,
    'COM_CAT#21': helpers.parseNumber,
    'COM_CAT#22': helpers.parseNumber,
    'COM_CAT#23': helpers.parseNumber,
    'COM_CAT#24': helpers.parseNumber,
    'COM_CAT#25': helpers.parseNumber,
    'COM_CAT#26': helpers.parseNumber,
    'COM_CAT#27': helpers.parseNumber,
    'COM_CAT#28': helpers.parseNumber,
    'COM_CAT#29': helpers.parseNumber,
    'COM_CAT#30': helpers.parseNumber,
    'COM_CAT#31': helpers.parseNumber,
    'COM_CAT#32': helpers.parseNumber,
    'COM_CAT#33': helpers.parseNumber,
    'COM_CAT#34': helpers.parseNumber,
    'SUM_MINUTES': helpers.parseNumber,
    'SUM_DATA_MB': helpers.parseNumber,
    'SUM_DATA_MIN': helpers.parseNumber,
    'CELL_LAC_ID_COUNT': helpers.parseNumber,
    'CELL_LAC_ID_UNIQ_COUNT': helpers.parseNumber,
    'CELL_LAC_ID_UNIQ_RATE': helpers.parseNumber,

    'CSI': Number
  };

  const featuresSet = helpers.readCSV(
    path.normalize(path.join(__dirname, './train_all.csv')),
    // SK_ID;CONTACT_DATE;SNAP_DATE;BASE_TYPE;ACT;ARPU_GROUP;DEVICE_TYPE_ID;INTERNET_TYPE_ID;REVENUE;ITC;VAS;RENT_CHANNEL;ROAM;COST;COM_CAT#1;COM_CAT#2;COM_CAT#3;COM_CAT#7;COM_CAT#8;COM_CAT#17;COM_CAT#18;COM_CAT#19;COM_CAT#20;COM_CAT#21;COM_CAT#22;COM_CAT#23;COM_CAT#24;COM_CAT#25;COM_CAT#26;COM_CAT#27;COM_CAT#28;COM_CAT#29;COM_CAT#30;COM_CAT#31;COM_CAT#32;COM_CAT#33;COM_CAT#34;SUM_MINUTES;SUM_DATA_MB;SUM_DATA_MIN;CELL_LAC_ID_COUNT;CELL_LAC_ID_UNIQ_COUNT;CELL_LAC_ID_UNIQ_RATE;CSI
    model
  ).filter(s => Boolean(s));

  const titles = Object.keys(model);

  const x = [];
  const y = [];

  for (const item of featuresSet) {
    const xi = [];

    for (const k of titles) {
      if (k !== 'CSI') {
        xi.push(item[k]);
      } else {
        y.push(item[k]);
      }
    }

    x.push(xi);
  }

  fs.writeFileSync(path.normalize(path.join(__dirname, './x_train.json')), JSON.stringify(x));
  fs.writeFileSync(path.normalize(path.join(__dirname, './y_train.json')), JSON.stringify(y));
}

{
  const model = {
    // 'SK_ID': Number,
    'CONTACT_DATE': helpers.convertDateToInt,
    'SNAP_DATE': helpers.convertDateToInt,
    'BASE_TYPE': Number,
    'ACT': helpers.parseNumber,
    'ARPU_GROUP': helpers.parseNumber,
    'DEVICE_TYPE_ID': Number,
    'INTERNET_TYPE_ID': Number,
    'REVENUE': helpers.parseNumber,
    'ITC': helpers.parseNumber,
    'VAS': helpers.parseNumber,
    'RENT_CHANNEL': helpers.parseNumber,
    'ROAM': helpers.parseNumber,
    'COST': helpers.parseNumber,
    'COM_CAT#1': helpers.parseNumber,
    'COM_CAT#2': helpers.parseNumber,
    'COM_CAT#3': helpers.parseNumber,
    'COM_CAT#7': helpers.parseNumber,
    'COM_CAT#8': helpers.parseNumber,
    'COM_CAT#17': helpers.parseNumber,
    'COM_CAT#18': helpers.parseNumber,
    'COM_CAT#19': helpers.parseNumber,
    'COM_CAT#20': helpers.parseNumber,
    'COM_CAT#21': helpers.parseNumber,
    'COM_CAT#22': helpers.parseNumber,
    'COM_CAT#23': helpers.parseNumber,
    'COM_CAT#24': helpers.parseNumber,
    'COM_CAT#25': helpers.parseNumber,
    'COM_CAT#26': helpers.parseNumber,
    'COM_CAT#27': helpers.parseNumber,
    'COM_CAT#28': helpers.parseNumber,
    'COM_CAT#29': helpers.parseNumber,
    'COM_CAT#30': helpers.parseNumber,
    'COM_CAT#31': helpers.parseNumber,
    'COM_CAT#32': helpers.parseNumber,
    'COM_CAT#33': helpers.parseNumber,
    'COM_CAT#34': helpers.parseNumber,
    'SUM_MINUTES': helpers.parseNumber,
    'SUM_DATA_MB': helpers.parseNumber,
    'SUM_DATA_MIN': helpers.parseNumber,
    'CELL_LAC_ID_COUNT': helpers.parseNumber,
    'CELL_LAC_ID_UNIQ_COUNT': helpers.parseNumber,
    'CELL_LAC_ID_UNIQ_RATE': helpers.parseNumber
  };

  const featuresSet = helpers.readCSV(
    path.normalize(path.join(__dirname, './test_all.csv')),
    // SK_ID;CONTACT_DATE;SNAP_DATE;BASE_TYPE;ACT;ARPU_GROUP;DEVICE_TYPE_ID;INTERNET_TYPE_ID;REVENUE;ITC;VAS;RENT_CHANNEL;ROAM;COST;COM_CAT#1;COM_CAT#2;COM_CAT#3;COM_CAT#7;COM_CAT#8;COM_CAT#17;COM_CAT#18;COM_CAT#19;COM_CAT#20;COM_CAT#21;COM_CAT#22;COM_CAT#23;COM_CAT#24;COM_CAT#25;COM_CAT#26;COM_CAT#27;COM_CAT#28;COM_CAT#29;COM_CAT#30;COM_CAT#31;COM_CAT#32;COM_CAT#33;COM_CAT#34;SUM_MINUTES;SUM_DATA_MB;SUM_DATA_MIN;CELL_LAC_ID_COUNT;CELL_LAC_ID_UNIQ_COUNT;CELL_LAC_ID_UNIQ_RATE;CSI
    model
  ).filter(s => Boolean(s));

  const titles = Object.keys(model);

  const x = [];
  const y = [];

  for (const item of featuresSet) {
    const xi = [];

    for (const k of titles) {
      if (k !== 'CSI') {
        xi.push(item[k]);
      } else {
        y.push(item[k]);
      }
    }

    x.push(xi);
  }

  fs.writeFileSync(path.normalize(path.join(__dirname, './x_test.json')), JSON.stringify(x));
}
