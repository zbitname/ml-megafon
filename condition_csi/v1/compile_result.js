const path = require('path');
const fs = require('fs');
const helpers = require('../../helpers');

const y = fs.readFileSync(path.normalize(path.join(__dirname, './y_test.csv'))).toString().split('\n');

const csi = fs.readFileSync(path.normalize(path.join(__dirname, './../csi_analyze/test/subs_csi_test.csv'))).toString().split('\n').map(v => Number(v.split(';')[0])).slice(1);

const x = helpers.readCSV(
  path.normalize(path.join(__dirname, './test_all.csv')),
  {
    'SK_ID': Number
  }
).map(v => v.SK_ID);

const result = [];

for (const skid of csi) {
  const idx = x.indexOf(skid);

  result.push(y[idx]);
}

fs.writeFileSync(path.normalize(path.join(__dirname, './y_test_final.csv')), result.join('\n'));
