const path = require('path');
const helpers = require('../../helpers');

const getDataSessions = pathStr => {
  return helpers.readCSV(
    path.normalize(path.join(__dirname, `./../csi_analyze/${pathStr}`)),
    {
      'SK_ID': Number,
      'CELL_LAC_ID': Number,
      'DATA_VOL_MB': helpers.parseNumber,
      'START_TIME': helpers.convertDateTimeHourToInt
    }
  ).filter(s => Boolean(s));
};

const getVoiceSessions = pathStr => {
  return helpers.readCSV(
    path.normalize(path.join(__dirname, `./../csi_analyze/${pathStr}`)),
    {
      'SK_ID': Number,
      'CELL_LAC_ID': Number,
      'VOICE_DUR_MIN': helpers.parseNumber,
      'START_TIME': helpers.convertDateTimeHourToInt
    }
  ).filter(s => Boolean(s));
};

// train
{
  const dataSessions = getDataSessions('train/subs_bs_data_session_train.csv');

  const groupedSessions = {};
  
  for (const r of dataSessions) {
    if (!groupedSessions[r.SK_ID]) {
      groupedSessions[r.SK_ID] = {};
    }

    if (!groupedSessions[r.SK_ID][r.CELL_LAC_ID]) {
      groupedSessions[r.SK_ID][r.CELL_LAC_ID] = {};
    }

    if (!groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME]) {
      groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME] = {};
    }

    groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME].DATA_VOL_MB = r.DATA_VOL_MB;
  }

  dataSessions.length = 0;

  const voiceSessions = getVoiceSessions('train/subs_bs_voice_session_train.csv');

  for (const r of voiceSessions) {
    if (!groupedSessions[r.SK_ID]) {
      groupedSessions[r.SK_ID] = {};
    }

    if (!groupedSessions[r.SK_ID][r.CELL_LAC_ID]) {
      groupedSessions[r.SK_ID][r.CELL_LAC_ID] = {};
    }

    if (!groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME]) {
      groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME] = {};
    }

    groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME].VOICE_DUR_MIN = r.VOICE_DUR_MIN;
  }

  voiceSessions.length = 0;

  const rows = [];

  for (const SK_ID in groupedSessions) {
    const cells = groupedSessions[SK_ID];

    for (const CELL_LAC_ID in cells) {
      const time = cells[CELL_LAC_ID];

      for (const START_TIME in time) {
        const r = time[START_TIME];

        rows.push(`${SK_ID};${CELL_LAC_ID};${START_TIME};${r.DATA_VOL_MB ? r.DATA_VOL_MB.toFixed(20).replace(/0+$/, '') : 0};${r.VOICE_DUR_MIN ? r.VOICE_DUR_MIN.toFixed(20).replace(/0+$/, '') : 0}`);
      }
    }
  }

  helpers.writeFromArraysCSV(path.normalize(path.join(__dirname, './data_and_voice_train.csv')), 'SK_ID;CELL_LAC_ID;START_TIME;DATA_VOL_MB;VOICE_DUR_MIN', rows);
}

// test
{
  const dataSessions = getDataSessions('test/subs_bs_data_session_test.csv');

  const groupedSessions = {};
  
  for (const r of dataSessions) {
    if (!groupedSessions[r.SK_ID]) {
      groupedSessions[r.SK_ID] = {};
    }

    if (!groupedSessions[r.SK_ID][r.CELL_LAC_ID]) {
      groupedSessions[r.SK_ID][r.CELL_LAC_ID] = {};
    }

    if (!groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME]) {
      groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME] = {};
    }

    groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME].DATA_VOL_MB = r.DATA_VOL_MB;
  }

  dataSessions.length = 0;

  const voiceSessions = getVoiceSessions('test/subs_bs_voice_session_test.csv');

  for (const r of voiceSessions) {
    if (!groupedSessions[r.SK_ID]) {
      groupedSessions[r.SK_ID] = {};
    }

    if (!groupedSessions[r.SK_ID][r.CELL_LAC_ID]) {
      groupedSessions[r.SK_ID][r.CELL_LAC_ID] = {};
    }

    if (!groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME]) {
      groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME] = {};
    }

    groupedSessions[r.SK_ID][r.CELL_LAC_ID][r.START_TIME].VOICE_DUR_MIN = r.VOICE_DUR_MIN;
  }

  voiceSessions.length = 0;

  const rows = [];

  for (const SK_ID in groupedSessions) {
    const cells = groupedSessions[SK_ID];

    for (const CELL_LAC_ID in cells) {
      const time = cells[CELL_LAC_ID];

      for (const START_TIME in time) {
        const r = time[START_TIME];

        rows.push(`${SK_ID};${CELL_LAC_ID};${START_TIME};${r.DATA_VOL_MB ? r.DATA_VOL_MB.toFixed(20).replace(/0+$/, '') : 0};${r.VOICE_DUR_MIN ? r.VOICE_DUR_MIN.toFixed(20).replace(/0+$/, '') : 0}`);
      }
    }
  }

  helpers.writeFromArraysCSV(path.normalize(path.join(__dirname, './data_and_voice_test.csv')), 'SK_ID;CELL_LAC_ID;START_TIME;DATA_VOL_MB;VOICE_DUR_MIN', rows);
}
