const path = require('path');
const fs = require('fs');
const helpers = require('../../helpers');

const y = fs.readFileSync(path.normalize(path.join(__dirname, './y_test.csv'))).toString().split('\n');

const csi = fs.readFileSync(path.normalize(path.join(__dirname, './../csi_analyze/test/subs_csi_test.csv'))).toString().split('\n').map(v => Number(v.split(';')[0])).slice(1);

const x = helpers.readCSV(
  path.normalize(path.join(__dirname, './test_all.csv')),
  {
    'SK_ID': Number
  }
).map(v => v.SK_ID);

const resultObj = {};

for (let i = 0; i < x.length; i++) {
  const skid = x[i];

  if (!resultObj[skid]) {
    resultObj[skid] = [];
  }

  resultObj[skid].push(Number(y[i]));
}

const result = [];

for (const skid of csi) {
  const row = resultObj[skid];

  // median
  // row.sort();
  // result.push((row[0] + row[row.length - 1]) / 2);

  // last
  // result.push(row[row.length - 1]);

  // first
  // result.push(row[0]);

  // avg
  // result.push(row.reduce((prev, cur) => prev + cur, 0) / row.length);

  // max
  row.sort();
  result.push(row[row.length - 1]);
  
  // min
  // row.sort();
  // result.push(row[0]);
}

fs.writeFileSync(path.normalize(path.join(__dirname, './y_test_final.csv')), result.join('\n'));
