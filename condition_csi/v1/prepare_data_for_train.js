// const fs = require('fs');
const path = require('path');
const helpers = require('../../helpers');

const getSubsCSI = pathStr => {
  return helpers.readCSV(path.normalize(path.join(__dirname, `./../csi_analyze/${pathStr}`)), {
    SK_ID: Number,
    CSI: Number,
    CONTACT_DATE: helpers.convertDateToInt
  }).filter(s => Boolean(s));
};

const getSubsFeatures = pathStr => {
  return helpers.readCSV(
    path.normalize(path.join(__dirname, `./../csi_analyze/${pathStr}`)),
    {
      'SNAP_DATE': helpers.convertDateToInt,
      'COM_CAT#1': helpers.parseNumber,
      'SK_ID': Number,
      'COM_CAT#2': helpers.parseNumber,
      'COM_CAT#3': helpers.parseNumber,
      'BASE_TYPE': helpers.parseNumber,
      'ACT': helpers.parseNumber,
      'ARPU_GROUP': helpers.parseNumber,
      'COM_CAT#7': helpers.parseNumber,
      'COM_CAT#8': helpers.parseNumber,
      'DEVICE_TYPE_ID': helpers.parseNumber,
      'INTERNET_TYPE_ID': helpers.parseNumber,
      'REVENUE': helpers.parseNumber,
      'ITC': helpers.parseNumber,
      'VAS': helpers.parseNumber,
      'RENT_CHANNEL': helpers.parseNumber,
      'ROAM': helpers.parseNumber,
      'COST': helpers.parseNumber,
      'COM_CAT#17': helpers.parseNumber,
      'COM_CAT#18': helpers.parseNumber,
      'COM_CAT#19': helpers.parseNumber,
      'COM_CAT#20': helpers.parseNumber,
      'COM_CAT#21': helpers.parseNumber,
      'COM_CAT#22': helpers.parseNumber,
      'COM_CAT#23': helpers.parseNumber,
      'COM_CAT#24': helpers.parseNumber,
      'COM_CAT#25': helpers.parseNumber,
      'COM_CAT#26': helpers.parseNumber,
      'COM_CAT#27': helpers.parseNumber,
      'COM_CAT#28': helpers.parseNumber,
      'COM_CAT#29': helpers.parseNumber,
      'COM_CAT#30': helpers.parseNumber,
      'COM_CAT#31': helpers.parseNumber,
      'COM_CAT#32': helpers.parseNumber,
      'COM_CAT#33': helpers.parseNumber,
      'COM_CAT#34': helpers.parseNumber
    }
  ).filter(s => Boolean(s));
};

const getSubsBSConsumption = pathStr => {
  return helpers.readCSV(
    path.normalize(path.join(__dirname, `./../csi_analyze/${pathStr}`)),
    {
      SK_ID: Number,
      CELL_LAC_ID: Number,
      MON: helpers.parseNumber,
      SUM_MINUTES: helpers.parseNumber,
      SUM_DATA_MB: helpers.parseNumber,
      SUM_DATA_MIN: helpers.parseNumber
    }
  ).filter(s => Boolean(s));
};

const getGroupedConsumptionSetSummaryBySK_ID = consumptionSet => {
  const consumptionSetSummaryBySK_ID = {};

  for (const consumption of consumptionSet) {
    if (consumptionSetSummaryBySK_ID[consumption.SK_ID]) {
      consumptionSetSummaryBySK_ID[consumption.SK_ID].CELL_LAC_IDS.push(consumption.CELL_LAC_ID);
      consumptionSetSummaryBySK_ID[consumption.SK_ID].SUM_MINUTES += consumption.SUM_MINUTES;
      consumptionSetSummaryBySK_ID[consumption.SK_ID].SUM_DATA_MB += consumption.SUM_DATA_MB;
      consumptionSetSummaryBySK_ID[consumption.SK_ID].SUM_DATA_MIN += consumption.SUM_DATA_MIN;
    } else {
      consumptionSetSummaryBySK_ID[consumption.SK_ID] = {
        CELL_LAC_IDS: [consumption.CELL_LAC_ID], // все id сот. Нужно чтобы потом выяснить разнообразие этих сот (сколько всего, доля уникальных и т.п.)
        SUM_MINUTES: consumption.SUM_MINUTES,
        SUM_DATA_MB: consumption.SUM_DATA_MB,
        SUM_DATA_MIN: consumption.SUM_DATA_MIN
      };
    }
  }

  return consumptionSetSummaryBySK_ID;
};

const injectCountUniqRate = consumptionSetSummaryBySK_ID => {
  for (const SK_ID in consumptionSetSummaryBySK_ID) {
    const set = consumptionSetSummaryBySK_ID[SK_ID];

    set.CELL_LAC_ID_COUNT = set.CELL_LAC_IDS.length;
    set.CELL_LAC_ID_UNIQ_COUNT = [...new Set(set.CELL_LAC_IDS)].length; // uniq via Set
    set.CELL_LAC_ID_UNIQ_RATE = set.CELL_LAC_ID_UNIQ_COUNT / set.CELL_LAC_ID_COUNT;

    delete set.CELL_LAC_IDS;
  }
};

const injectSumsMinutesDataMBDataMin = (featuresSet, groupedBySK_ID, consumptionSetSummaryBySK_ID) => {
  for (const set of featuresSet) {
    set.CSI = groupedBySK_ID[set.SK_ID].CSI;
    set.CONTACT_DATE = groupedBySK_ID[set.SK_ID].CONTACT_DATE;

    if (consumptionSetSummaryBySK_ID[set.SK_ID]) {
      set.SUM_MINUTES = consumptionSetSummaryBySK_ID[set.SK_ID].SUM_MINUTES;
      set.SUM_DATA_MB = consumptionSetSummaryBySK_ID[set.SK_ID].SUM_DATA_MB;
      set.SUM_DATA_MIN = consumptionSetSummaryBySK_ID[set.SK_ID].SUM_DATA_MIN;

      set.CELL_LAC_ID_COUNT = consumptionSetSummaryBySK_ID[set.SK_ID].CELL_LAC_ID_COUNT;
      set.CELL_LAC_ID_UNIQ_COUNT = consumptionSetSummaryBySK_ID[set.SK_ID].CELL_LAC_ID_UNIQ_COUNT;
      set.CELL_LAC_ID_UNIQ_RATE = consumptionSetSummaryBySK_ID[set.SK_ID].CELL_LAC_ID_UNIQ_RATE;
    } else {
      set.SUM_MINUTES = 0;
      set.SUM_DATA_MB = 0;
      set.SUM_DATA_MIN = 0;

      set.CELL_LAC_ID_COUNT = 0;
      set.CELL_LAC_ID_UNIQ_COUNT = 0;
      set.CELL_LAC_ID_UNIQ_RATE = 0;
    }
  }
};

{
  const trainSet = getSubsCSI('train/subs_csi_train.csv');

  const groupedBySK_ID = {};

  for (const set of trainSet) {
    groupedBySK_ID[set.SK_ID] = {
      CSI: set.CSI,
      CONTACT_DATE: set.CONTACT_DATE
    };
  }

  const featuresSet = getSubsFeatures('train/subs_features_train.csv');

  // SK_ID;CELL_LAC_ID;MON;SUM_MINUTES;SUM_DATA_MB;SUM_DATA_MIN
  const consumptionSet = getSubsBSConsumption('train/subs_bs_consumption_train.csv');

  const consumptionSetSummaryBySK_ID = getGroupedConsumptionSetSummaryBySK_ID(consumptionSet);

  injectCountUniqRate(consumptionSetSummaryBySK_ID);

  // extend featuresSet
  injectSumsMinutesDataMBDataMin(featuresSet, groupedBySK_ID, consumptionSetSummaryBySK_ID);

  helpers.writeCSV(
    path.normalize(path.join(__dirname, './train_all.csv')),
    [
      'SK_ID',
      'CONTACT_DATE',
      'SNAP_DATE',
      'BASE_TYPE',
      'ACT',
      'ARPU_GROUP',
      'DEVICE_TYPE_ID',
      'INTERNET_TYPE_ID',
      'REVENUE',
      'ITC',
      'VAS',
      'RENT_CHANNEL',
      'ROAM',
      'COST',
      'COM_CAT#1',
      'COM_CAT#2',
      'COM_CAT#3',
      'COM_CAT#7',
      'COM_CAT#8',
      'COM_CAT#17',
      'COM_CAT#18',
      'COM_CAT#19',
      'COM_CAT#20',
      'COM_CAT#21',
      'COM_CAT#22',
      'COM_CAT#23',
      'COM_CAT#24',
      'COM_CAT#25',
      'COM_CAT#26',
      'COM_CAT#27',
      'COM_CAT#28',
      'COM_CAT#29',
      'COM_CAT#30',
      'COM_CAT#31',
      'COM_CAT#32',
      'COM_CAT#33',
      'COM_CAT#34',
      'SUM_MINUTES',
      'SUM_DATA_MB',
      'SUM_DATA_MIN',
      'CELL_LAC_ID_COUNT',
      'CELL_LAC_ID_UNIQ_COUNT',
      'CELL_LAC_ID_UNIQ_RATE',

      'CSI'
    ],
    featuresSet
  );
}

{
  const trainSet = getSubsCSI('test/subs_csi_test.csv');

  const groupedBySK_ID = {};

  for (const set of trainSet) {
    groupedBySK_ID[set.SK_ID] = {
      CONTACT_DATE: set.CONTACT_DATE
    };
  }

  const featuresSet = getSubsFeatures('test/subs_features_test.csv');

  // SK_ID;CELL_LAC_ID;MON;SUM_MINUTES;SUM_DATA_MB;SUM_DATA_MIN
  const consumptionSet = getSubsBSConsumption('test/subs_bs_consumption_test.csv');

  const consumptionSetSummaryBySK_ID = getGroupedConsumptionSetSummaryBySK_ID(consumptionSet);

  injectCountUniqRate(consumptionSetSummaryBySK_ID);

  // extend featuresSet
  injectSumsMinutesDataMBDataMin(featuresSet, groupedBySK_ID, consumptionSetSummaryBySK_ID);

  helpers.writeCSV(
    path.normalize(path.join(__dirname, './test_all.csv')),
    [
      'SK_ID',
      'CONTACT_DATE',
      'SNAP_DATE',
      'BASE_TYPE',
      'ACT',
      'ARPU_GROUP',
      'DEVICE_TYPE_ID',
      'INTERNET_TYPE_ID',
      'REVENUE',
      'ITC',
      'VAS',
      'RENT_CHANNEL',
      'ROAM',
      'COST',
      'COM_CAT#1',
      'COM_CAT#2',
      'COM_CAT#3',
      'COM_CAT#7',
      'COM_CAT#8',
      'COM_CAT#17',
      'COM_CAT#18',
      'COM_CAT#19',
      'COM_CAT#20',
      'COM_CAT#21',
      'COM_CAT#22',
      'COM_CAT#23',
      'COM_CAT#24',
      'COM_CAT#25',
      'COM_CAT#26',
      'COM_CAT#27',
      'COM_CAT#28',
      'COM_CAT#29',
      'COM_CAT#30',
      'COM_CAT#31',
      'COM_CAT#32',
      'COM_CAT#33',
      'COM_CAT#34',
      'SUM_MINUTES',
      'SUM_DATA_MB',
      'SUM_DATA_MIN',
      'CELL_LAC_ID_COUNT',
      'CELL_LAC_ID_UNIQ_COUNT',
      'CELL_LAC_ID_UNIQ_RATE'
    ],
    featuresSet
  );
}
