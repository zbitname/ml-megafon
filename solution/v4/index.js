const path = require('path');
const helpers = require('../../helpers');

const step1 = require('../../analyze/grouped/step1');

const resultData = helpers.readCSV(
  path.normalize(path.join(__dirname, '../../condition_csi/csi_analyze/test/subs_csi_test.csv')),
  {
    SK_ID: Number,
    CONTACT_DATE: String
  }
);

const testData = helpers.readCSV(path.normalize(path.join(__dirname, '../../condition_csi/csi_analyze/test/subs_features_test.csv')),
{
  'SNAP_DATE': String,
  'COM_CAT#1': helpers.parseNumber,
  'SK_ID': Number,
  'COM_CAT#2': helpers.parseNumber,
  'COM_CAT#3': helpers.parseNumber,
  'BASE_TYPE': helpers.parseNumber,
  'ACT': helpers.parseNumber,
  'ARPU_GROUP': helpers.parseNumber,
  'COM_CAT#7': helpers.parseNumber,
  'COM_CAT#8': helpers.parseNumber,
  'DEVICE_TYPE_ID': helpers.parseNumber,
  'INTERNET_TYPE_ID': helpers.parseNumber,
  'REVENUE': helpers.parseNumber,
  'ITC': helpers.parseNumber,
  'VAS': helpers.parseNumber,
  'RENT_CHANNEL': helpers.parseNumber,
  'ROAM': helpers.parseNumber,
  'COST': helpers.parseNumber,
  'COM_CAT#17': helpers.parseNumber,
  'COM_CAT#18': helpers.parseNumber,
  'COM_CAT#19': helpers.parseNumber,
  'COM_CAT#20': helpers.parseNumber,
  'COM_CAT#21': helpers.parseNumber,
  'COM_CAT#22': helpers.parseNumber,
  'COM_CAT#23': helpers.parseNumber,
  'COM_CAT#24': helpers.parseNumber,
  'COM_CAT#25': helpers.parseNumber,
  'COM_CAT#26': helpers.parseNumber,
  'COM_CAT#27': helpers.parseNumber,
  'COM_CAT#28': helpers.parseNumber,
  'COM_CAT#29': helpers.parseNumber,
  'COM_CAT#30': helpers.parseNumber,
  'COM_CAT#31': helpers.parseNumber,
  'COM_CAT#32': helpers.parseNumber,
  'COM_CAT#33': helpers.parseNumber,
  'COM_CAT#34': helpers.parseNumber
});

const metrics = {
  nulls: 0,
  negative: 0,
  positive: 0
}

for (const row of resultData) {
  const r = step1.getResult(testData.filter(r => r.SK_ID === row.SK_ID));

  row.CSI = r;

  if (r === null) {
    row.CSI = 0;
    metrics.nulls++;
  } else if (r > .5) {
    metrics.negative++;
  } else if (r <= .5) {
    metrics.positive++;
  }
}

console.log(metrics);

helpers.buildResultCSV(path.normalize(path.join(__dirname, 'subs.csi.test.csv')), resultData);
