// difference for values (outersection)

const path = require('path');
const fs = require('fs');

const data = require('../result.json');

const result = {};

const BORDER = 9;

for (const paramName in data) {
  const itemData = data[paramName];

  const negativeValues = itemData['0'];
  const positiveValues = itemData['1'];

  result[paramName] = {0: [], 1: []};

  for (const val in negativeValues) {
    if (!positiveValues[val] && negativeValues[val] >= BORDER) {
      result[paramName]['0'].push(val);
    }
  }

  for (const val in positiveValues) {
    if (!negativeValues[val] && positiveValues[val] >= BORDER) {
      result[paramName]['1'].push(val);
    }
  }
}

fs.writeFileSync(path.normalize(path.join(__dirname, './result.json')), JSON.stringify(result, null, 2));
