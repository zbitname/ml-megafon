const groupedData = require('./result.json');

module.exports.getResult = rows => {
  const result = [];

  for (const row of rows) {
    for (const name in row) {
      if (name === 'SK_ID') {
        continue;
      }

      if (groupedData[name]['0'].indexOf(String(row[name])) >= 0) {
        result.push(0);
      }
  
      if (groupedData[name]['1'].indexOf(String(row[name])) >= 0) {
        result.push(1);
      }
    }
  }

  if (result.every(v => v === 0)) {
    return 0;
  }

  if (result.every(v => v === 1)) {
    return 1;
  }

  if (result.filter(v => v === 0).length > result.filter(v => v === 1).length) {
    return 0;
  }

  if (result.filter(v => v === 0).length < result.filter(v => v === 1).length) {
    return 1;
  }

  return null;
};
