const groupedData = require('./result.json');

const BORDER = .21;

module.exports.getResult = rows => {
  const results = [];

  for (const row of rows) {
    const resultForRow = [];

    for (const name in row) {
      if (name === 'SK_ID') {
        continue;
      }

      if (groupedData[name]['0'].indexOf(String(row[name])) >= 0) {
        resultForRow.push(0);
      }
  
      if (groupedData[name]['1'].indexOf(String(row[name])) >= 0) {
        resultForRow.push(1);
      }
    }

    if (resultForRow.length) {
      results.push(resultForRow.reduce((prev, cur) => prev + cur, 0) / resultForRow.length);
    }
  }

  if (!results.length) {
    return null;
  }

  const rate = results.reduce((prev, cur) => prev + cur, 0) / results.length;

  if (rate > BORDER) {
    return 1;
  }

  if (rate < BORDER) {
    return 0;
  }

  return null;
};
